module.exports = function (grunt) {
	grunt.initConfig ({
		pkg: grunt.file.readJSON ("package.json"),
		uglify: {
			finn_server: {
				files: {
					"dist/finn.server.min.js": [
						"src/router.js",
						"src/cache.js"
					]
				}
			}
		},
		jshint: {
			finn_server: {
				src: [
					"src/router.js",
					"src/cache.js"
				]
			}
		}
	});
	
	grunt.loadNpmTasks ("grunt-contrib-jshint");
	grunt.loadNpmTasks ("grunt-contrib-uglify");
};
