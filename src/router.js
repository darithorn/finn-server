var exports = exports || {};
var http = require ("http");
var fs = require ("fs");
var url = require ("url");
var qs = require ("querystring");

function getContentType (filename) {
	var htmlExp = new RegExp (".*\.html|mst");
	if (htmlExp.exec (filename) !== null) {
		return "text/html";
	}
	var jsExp = new RegExp (".*\.js");
	if (jsExp.exec (filename) !== null) {
		return "text/javascript";
	}
	var cssExp = new RegExp (".*\.css");
	if (cssExp.exec (filename) !== null) {
		return "text/css";
	}
	var svgExp = new RegExp (".*\.svg");
	if (svgExp.exec (filename) !== null) {
		return "image/svg+xml";
	}
	return "text/plain";
}

exports.Router = function (hash) {
	hash = hash || {};
	this.ip = 0;
	this.routes = {};
	this.directory = "";
	this.cache = null;
	// The max amount of data the router will accept
	// for a POST method
	this.postMax = 512;
	
	this._server = http.createServer ();

	if (hash == null) {
		hash = {};
	}
	for (var key in hash) {
		this[key] = hash[key];
	}

	if (this.directory[0] !== '/') {
		this.directory = '/' + this.directory;
	}

	var that = this;
	this._server.on ("request", function (req, res) {
		that.match (req, res);
	});
	this._server.listen (this.ip);
};

exports.Router.prototype.defaultRoute = function (req, res) {
	if (req.url[0] === "/") {
		// Trim the beginning / if there is one
		req.url = req.url.slice (1);
	}
	// Use the cache if it's provided
	if (this.cache != null) {
		var requested_file = this.cache.dir + req.url;
		if (this.cache.assets[requested_file] != undefined) {
			res.writeHead (200, { "content-type": getContentType (req.url) });
			res.write (this.cache.assets[requested_file]);
			res.end ();
			return;
		}
	}
	// If no cache then load the file
	fs.readFile ("." + this.directory + req.url, function (err, file) {
		if (err) {
			// This is not a fatal error so no need to `throw`
			console.error (err);
			res.writeHeader (404, { "Content-Type": "text/plain" });
			res.end ();
		} else {
			res.writeHeader (200, { "Content-Type": getContentType (req.url) });
			res.write (file);
			res.end ();
		}
	});
};

exports.Router.prototype.match = function (req, res) {
	if (req.url[req.url.length - 1] === "?") {
		// If there's a ? at the end, get rid of it.
		// It complicates route expressions.
		req.url = req.url.slice (0, req.url.length - 1);
	}
	var matched = false;
	for (var key in this.routes) {
		var regex = new RegExp ("^" + key + "$");
		var result = regex.exec (req.url);
		if (result !== null) {
			req.result = result;
			if (req.method === "POST") {
				var that = this;
				var body = "";
				req.on ("data", function (data) {
					body += data;
					if (body.length > that.postData) {
						req.connection.destroy ();
					}
				});
				req.on ("end", function () {
					req.post = qs.parse (body);
					that.routes [key] (req, res);
				});
			} else {
				this.routes[key] (req, res);
			}
			matched = true;
			break;
		}
	}
	if (!matched) {
		this.defaultRoute (req, res);
	}
};

exports.Router.prototype.redirect = function (url, res) {
	res.writeHead (301, {
		"location": url
	});
	res.end ();
};

// callback (request, response)
exports.Router.prototype.route = function (url, callback) {
	// /test/{{0}}/
	var exp = new RegExp ("{{([0-9]+)}}", "g");
	url = url.replace (exp, "(\\w+)");
	this.routes[url] = callback;
};
