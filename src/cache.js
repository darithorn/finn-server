var exports = exports || {};
var fs = require ("fs");

exports.Cache = function (ext) {
	this.dir = "";

	this.fileExp = new RegExp ("([^0-9]\\w*)\.(?:.+[^~])$");
	this.fileStats = {};
	this.assets = {};

	if (ext == null) {
		ext = {};
	}
	for (var i in ext) {
		this[i] = ext[i];
	}
	if (this.dir [this.dir.length - 1] !== "/") {
		// Make sure there is a / at the end of the directory
		this.dir += "/";
	}
	this.cacheFiles (this.dir);
};

exports.Cache.prototype.readFile = function (filename, callback) {
	fs.readFile (filename, { encoding: "utf-8" },
		function (err, data) {
			if (err) {
				throw err;
			} else {
				callback (data);
			}
		});
};

exports.Cache.prototype.get = function (filename) {
	var cache = this.assets[filename];
	return cache || "";
};

exports.Cache.prototype.cacheFiles = function (dir) {
	var that = this;
	fs.readdir (dir, function (err, files) {
		if (err) {
			throw err;
		}
		for (var i = 0; i < files.length; i++) {
			var result = that.fileExp.exec (files[i]);
			if (result) {
				(function (name) {
					fs.stat (dir + name, function (err, stats) {
						if (err) {
							throw err;
						}
						if (stats.isFile ()) {
							that.readFile (dir + name, function (data) {
								if (data) {
									that.assets[dir + name] = data;
									that[dir + name] = data;
								}
							});
						} else if(stats.isDirectory ()) {
							that.cacheFiles (dir + name + "/");
						}
						that.fileStats[dir + name] = stats;
					});
				}) (files[i]);
			}
		}
	});
};
