var finn = require ("../dist/finn.server.min.js");

var cache = new finn.Cache ({
	dir: "assets/"
});

var router = new finn.Router ({
	ip: 8888,
	cache: cache
});

router.route ("/", function (req, res) {
	res.writeHead (200);
	res.write (cache["assets/templates/index.html"]);
	res.end ();
});

router.route ("/test/{{0}}?", function (req, res) {
	res.writeHead (200);
	res.write (cache["assets/templates/index.html"]);
	res.end ();	
});
